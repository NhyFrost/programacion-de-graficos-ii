#include "stdafx.h"
#include "Game.h"
#include "Input.h"
#include "WorldMatrixManager.h"
#include "ViewMatrixManager.h"

using namespace std::chrono;

#define CUSTOMFVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)

//Pongo el nombre de la funcion con sus parametros sin su contenido arriba para que el compilador sepa que exista una funcion asi
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

Game* Game::Instance = nullptr;


Game::Game(HINSTANCE hInstance)
//Crear la clase de la ventana y registrarla
//Create la ventana y guardar el handler
{
	Instance = this;

	width = 800;
	height = 600;

	_hInstance = hInstance;
	//Creamos la clase de la ventana
	WNDCLASSEX wcex;
	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

	//Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	_hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"Engine", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		width, //Ancho
		height, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		_hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);
	_d3d = d3d;

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = _hWnd;
	d3dpp.BackBufferWidth = width;
	d3dpp.BackBufferHeight = height;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferFormat = D3DFMT_R5G6B5;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	//Creo la interfaz con la placa de video
	HRESULT hr = _d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de vido
		D3DDEVTYPE_HAL, //Soft o hard
		_hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&_dev); //El device que se crea

	if (hr != D3D_OK)
	{
		return;
	}

	_dev->SetRenderState(D3DRS_LIGHTING, false);
	_dev->SetRenderState(D3DRS_ZENABLE, TRUE);
	//_dev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	_dev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); //desactiva el culling(para ver las 2 caras de un plano)

	WorldMM = new WorldMatrixManager();
	ViewMM = new ViewMatrixManager();
}


Game::~Game()
{

}

void Game::Start(int nCmdShow)
//Mostrar la ventana
//Inicia el bucle principal
{

	_nCmdShow = nCmdShow;
	ShowWindow(_hWnd, _nCmdShow); //Muestro la ventana
	UpdateWindow(_hWnd); //La actualizo para que se vea




	long tiempoAnterior = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

	Input::InitInputKeyboard();

	while (true)
	{
		Input::UpdateKeyboard();

		MSG msg;

		long tiempoActual = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		deltaTime = (tiempoActual - tiempoAnterior) / 1000.0f;
		tiempoAnterior = tiempoActual;

		//Saco un mensaje de la cola de mensajes si es que hay
		//sino continuo
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}


		//Actualizar
		Update();

		_dev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(255, 255, 150, 150), 1.0f, 0);
		_dev->BeginScene();

		//Dibujar
		Draw();

		_dev->EndScene();
		_dev->Present(NULL, NULL, NULL, NULL);


	}
	Input::FreeResources();
	_dev->Release();
	_d3d->Release();
}

void Game::Update()
{
	currentScene->Update();
}

void Game::Draw()
{
	currentScene->Draw();
}

/*void Game::Update()
{
	for (size_t i = 0; i < actores.size(); i++)
		actores[i]->Update();


	currentScene->Update();
}

void Game::Draw()
{
	for (size_t i = 0; i < cameras.size(); i++)
	{
		cameras[i]->SetTransform();
		cameras[i]->SetRotation();
		for (size_t i = 0; i < actores.size(); i++)
		{
			actores[i]->Draw();
		}
	}

	currentScene->Draw();

}*/

//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}