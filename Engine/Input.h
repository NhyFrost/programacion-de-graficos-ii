#pragma once
#include "EngineApi.h"

class ENGINE_API Input
{
public:
	static LPDIRECTINPUT8 dinp;
	static LPDIRECTINPUTDEVICE8 keyDev;

	static std::map<std::string, std::vector<int>> _keys;

	static byte prevKeys[256];
	static byte currentKeys[256];

	static void InitInputKeyboard();
	static void SetKey(std::string actionName, int key);
	static bool GetKey(std::string actionName);
	static bool GetKeyDown(std::string actionName);
	static bool GetKeyUp(std::string actionName);
	static void UpdateKeyboard();
	static void FreeResources();
};

