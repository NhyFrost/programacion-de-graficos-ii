#include "stdafx.h"
#include "Importer.h"

using namespace std;

bool Importer::LoadOBJ(const char* path, Mesh** m)
{
	vector<D3DXVECTOR3> temp_vertexes;
	vector<D3DXVECTOR2> temp_UVs;
	vector<D3DXVECTOR3> temp_normals;
	vector<D3DXVECTOR3> temp_indexes;

	FILE * file = NULL;

	if (fopen_s(&file, path, "r") != 0)
	{
		printf("Impossible to open the file !\n");
		return false;
	}

	while (1)
	{
		char lineHeader[128];
		// Lee la primera palabra de la l�nea
		int res = fscanf_s(file, "%s", lineHeader, 128);
		if (res == EOF)
			break; // EOF = End Of File, es decir, el final del archivo. Se finaliza el ciclo.

		// else : analizar el lineHeader

		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 vertex;
			fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertexes.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf_s(file, "%f %f\n", &uv.x, &uv.y);
			temp_UVs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf_s(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			D3DXVECTOR3 index[3];

			int matches = fscanf_s(file, "%f/%f/%f %f/%f/%f %f/%f/%f\n", &index[0].x, &index[0].y, &index[0].z,
				&index[1].x, &index[1].y, &index[1].z,
				&index[2].x, &index[2].y, &index[2].z);

			if (matches != 9)
			{
				printf("File can't be read by our simple parser : ( Try exporting with other options\n");
				return false;
			}

			temp_indexes.push_back(index[0]);
			temp_indexes.push_back(index[1]);
			temp_indexes.push_back(index[2]);
		}
	}//end while

	for (int i = 0; i < temp_indexes.size(); i++)
	{
		int idx = -1;

		D3DXVECTOR3 auxVertex = temp_vertexes[(temp_indexes[i].x)-1];
		D3DXVECTOR2 auxUV = temp_UVs[(temp_indexes[i].y)-1];
		D3DXVECTOR3 auxNormal = temp_normals[(temp_indexes[i].z)-1];

		for (int j = 0; j < finalVertexes.size(); j++)
		{

			if (finalVertexes[j].coord3D == auxVertex && finalVertexes[j].coord2D == auxUV)
			{
				idx = j;
				break;
			}
		}

		if (idx == -1)
		{
			Vertex v(auxVertex.x, auxVertex.y, auxVertex.z, auxUV.x, auxUV.y);
			idx = finalVertexes.size(); // le paso el indice del tama;o ( si tiene 0 elementos paso indice 0, si tiene 1 elemento paso indice 1)
			finalVertexes.push_back(v); // despues lo agrego ( si tenia 0 elementos, ahora tiene 1, pero su indice es 0)
		}

		indexes.push_back(idx);
	}

	int vLenght = finalVertexes.size();
	Vertex* v = new Vertex[vLenght];

	for (int i = 0; i < vLenght; i++)
	{
		v[i] = finalVertexes[i];
	}

	int iLenght = indexes.size();

	WORD* ind = new WORD[iLenght];

	for (int i = 0; i < iLenght; i++)
	{
		ind[i] = indexes[i];
	}

	*m = new Mesh(v, vLenght, ind, iLenght);

	return true;

}