#include "stdafx.h"
#include "Transform.h"

Transform::Transform() : pos(0, 0, 0), rot(0, 0, 0), sca(1, 1, 1), forward(0, 0, 1), up(0, 1, 0), right(1, 0, 0)
{
}


Transform::~Transform()
{
}

D3DXMATRIX Transform::GetModel()
{
	D3DXMATRIX _matTrans;
	D3DXMATRIX _matScale;
	D3DXMATRIX _matRotX;
	D3DXMATRIX _matRotY;
	D3DXMATRIX _matRotZ;
	D3DXMatrixTranslation(&_matTrans, pos.x, pos.y, pos.z);
	D3DXMatrixScaling(&_matScale, sca.x, sca.y, sca.z);
	D3DXMatrixRotationX(&_matRotX, D3DXToRadian(rot.x));
	D3DXMatrixRotationY(&_matRotY, D3DXToRadian(rot.y));
	D3DXMatrixRotationZ(&_matRotZ, D3DXToRadian(rot.z));

	return _matTrans*_matRotX*_matRotY*_matRotZ *_matScale;
}

void Transform::Translate(float x, float y, float z)
{
	pos += ((right * x) + (up * y) + (forward * z));

	/*pos.x += x;
	pos.y += y;		//transform world
	pos.z += z;*/
}

void Transform::Scale(float x, float y, float z) {
	sca.x += x;
	sca.y += y;
	sca.z += z;
}

void Transform::Rotate(float x, float y, float z)
{
	D3DXMATRIX mat;

	D3DXMatrixRotationAxis(&mat, &up, y);
	D3DXVec3TransformCoord(&forward, &forward, &mat);
	D3DXVec3TransformCoord(&right, &right, &mat);

	D3DXMatrixRotationAxis(&mat, &forward, z);
	D3DXVec3TransformCoord(&up, &up, &mat);
	D3DXVec3TransformCoord(&right, &right, &mat);

	D3DXMatrixRotationAxis(&mat, &right, x);
	D3DXVec3TransformCoord(&up, &up, &mat);
	D3DXVec3TransformCoord(&forward, &forward, &mat);

	rot.x += x;
	rot.y += y;
	rot.z += z;

	if (rot.x >= 360) rot.x - 360;
	if (rot.z >= 360) rot.z - 360;
	if (rot.y >= 360) rot.y - 360;
	if (rot.x < 0)	  rot.x + 360;
	if (rot.z < 0)	  rot.z + 360;
	if (rot.y < 0)	  rot.y + 360;
}

/*

void Transform::Transformar() {
_mat= _matTrans*_matScale*_matRotX*_matRotY*_matRotZ;
Game::Instance->_dev->SetTransform(D3DTS_WORLD, &_mat);
}

*/