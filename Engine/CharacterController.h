#ifndef CHARACTERCONTROLLER_H
#define CHARACTERCONTORLLER_H

#include "Component.h"
#include "EngineApi.h"

class ENGINE_API CharacterController : public Component
{
public: 
	float movementSpeed;
	float rotationSpeed;
	CharacterController();
	void Update() override;
};



#endif //CHARACTERCONTROLLER_H