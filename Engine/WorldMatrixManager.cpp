#include "stdafx.h"
#include "WorldMatrixManager.h"
#include "Game.h"

void WorldMatrixManager::MultiplyTransform(D3DXMATRIX* m)
{
	Game::Instance->_dev->MultiplyTransform(D3DTS_WORLD, m);
}

void WorldMatrixManager::SetTransform(D3DXMATRIX* m)
{
	Game::Instance->_dev->SetTransform(D3DTS_WORLD, m);
}

void WorldMatrixManager::Reset()
{
	//D3DXMATRIX* aux = new D3DXMATRIX();

	Matrices.clear();
	D3DXMatrixIdentity(aux);

	Game::Instance->_dev->SetTransform(D3DTS_WORLD, aux);
}