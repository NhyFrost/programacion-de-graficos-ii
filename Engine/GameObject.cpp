#include "stdafx.h"
#include "GameObject.h"
#include "Game.h"

void GameObject::Awake()
{
	isUpdating = true;
}

void GameObject::Update()
{
	for each (Node* child in children)
	{
		if (!child->isUpdating)
		{
			child->Awake();
		}
		else
		{
			child->Update();
		}
	}
}

void GameObject::Sleep()
{

}

void GameObject::Draw()
{
	Game::Instance->WorldMM->MultiplyTransform(&transform->GetModel());

	for each (Node* child in children)
	{
		Game::Instance->WorldMM->Push(&transform->GetModel());
		child->Draw();
		Game::Instance->WorldMM->Pop();
	}
}

Node* GameObject::GetChildByName(string name)
{
	for (int i = 0; i < children.size(); i++)
	{
		if (children[i]->name == name)
			return children[i];
	}

	return NULL;
}