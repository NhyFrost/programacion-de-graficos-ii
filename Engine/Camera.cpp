#include "stdafx.h"
#include "Game.h"
#include "Camera.h"


Camera::Camera() : _FOV(60), _start(0), _end(10)
{
	for (int i = 0; i < 6; i++)
	{
		D3DXPLANE* plane = new D3DXPLANE();
		frustrum.push_back(plane);
	}

	for (int i = 0; i < 5; i++)
	{
		D3DXVECTOR4* v = new D3DXVECTOR4(0, 0, 0, 0);
		frustrumVertexes.push_back(v);
	}

	for (int i = 0; i < 8; i++)
	{
		D3DXVECTOR4* v = new D3DXVECTOR4(0, 0, 0, 0);
		BoxfrustrumVertexes.push_back(v);
	}

	/*float adjacent = _end;

	float opposite = tanf(D3DXToRadian(_FOV)) * adjacent;

	float sum = (powf(adjacent, 2) + powf(opposite, 2));

	float hypotenuse = sqrtf(sum);

	D3DXMATRIX mat;

	D3DXVECTOR3 forward(0, 0, 1);
	D3DXVECTOR3 up(0, 1, 0);

	D3DXMatrixRotationAxis(&mat, &up, D3DXToRadian(_FOV));
	D3DXVec3TransformCoord(&forward, &forward, &mat);

	middleRightPoint = forward*hypotenuse;*/

	float height = 2.0f * _end * tan(D3DXToRadian(_FOV * 0.5f));
	float width = height * Game::Instance->width / Game::Instance->height;
	float length = _end;

	maximum = new D3DXVECTOR4;
	minimum = new D3DXVECTOR4;

	frustrumCenter = new D3DXVECTOR4(0, 0, 0, 0);

	//D3DXVec3Transform(frustrumCenter, (D3DXVECTOR3*)frustrumCenter, Game::Instance->WorldMM->GetCurrentMatrix()); //
}

Camera::~Camera()
{

}

void Camera::Draw()
{
	D3DXMATRIX matView;
	D3DXMatrixLookAtLH(&matView, &((GameObject*)parent)->transform->pos, &(((GameObject*)parent)->transform->pos + ((GameObject*)parent)->transform->forward), &((GameObject*)parent)->transform->up);
	Game::Instance->_dev->SetTransform(D3DTS_VIEW, &matView);

	D3DXMATRIX projView;
	D3DXMatrixPerspectiveFovLH(&projView, D3DXToRadian(_FOV), Game::Instance->width / (float)Game::Instance->height, _start, _end);
	Game::Instance->_dev->SetTransform(D3DTS_PROJECTION, &projView);


	D3DXMatrixMultiply(&projView, &matView, &projView);


	//left plane
	frustrum[0]->a = projView._14 + projView._11;
	frustrum[0]->b = projView._24 + projView._21;
	frustrum[0]->c = projView._34 + projView._31;
	frustrum[0]->d = projView._44 + projView._41;

	//right plane
	frustrum[1]->a = projView._14 - projView._11;
	frustrum[1]->b = projView._24 - projView._21;
	frustrum[1]->c = projView._34 - projView._31;
	frustrum[1]->d = projView._44 - projView._41;

	//top plane
	frustrum[2]->a = projView._14 - projView._12;
	frustrum[2]->b = projView._24 - projView._22;
	frustrum[2]->c = projView._34 - projView._32;
	frustrum[2]->d = projView._44 - projView._42;

	//bottom plane
	frustrum[3]->a = projView._14 + projView._12;
	frustrum[3]->b = projView._24 + projView._22;
	frustrum[3]->c = projView._34 + projView._32;
	frustrum[3]->d = projView._44 + projView._42;

	//near plane
	frustrum[4]->a = projView._13;
	frustrum[4]->b = projView._23;
	frustrum[4]->c = projView._33;
	frustrum[4]->d = projView._43;

	//far plane
	frustrum[5]->a = projView._14 - projView._13;
	frustrum[5]->b = projView._24 - projView._23;
	frustrum[5]->c = projView._34 - projView._33;
	frustrum[5]->d = projView._44 - projView._43;

	// Normalize planes
	for (int i = 0; i < 6; i++)
	{
		D3DXPlaneNormalize(frustrum[i], frustrum[i]);
	}

	((Scene*)parent->rootParent)->mainCamera = this;
	
	float height = 2.0f * _end * tan(D3DXToRadian(_FOV * 0.5f));
	float width = height * (float)Game::Instance->width / (float)Game::Instance->height;
	float length = _end;

	frustrumVertexes[0] = new D3DXVECTOR4(0,0,0,0);

	frustrumVertexes[1]->x = width;
	frustrumVertexes[1]->y = height;
	frustrumVertexes[1]->z = length;	

	frustrumVertexes[2]->x = -width;
	frustrumVertexes[2]->y = -height;
	frustrumVertexes[2]->z = length;		

	frustrumVertexes[3]->x = -width;
	frustrumVertexes[3]->y = height;
	frustrumVertexes[3]->z = length;	

	frustrumVertexes[4]->x = width;
	frustrumVertexes[4]->y = -height;
	frustrumVertexes[4]->z = length;		

	D3DXVec3Transform(frustrumVertexes[0], (D3DXVECTOR3*)frustrumVertexes[0], Game::Instance->WorldMM->GetCurrentMatrix());
	float max = std::numeric_limits<float>::infinity();
	float min = -std::numeric_limits<float>::infinity();

	maximum = new D3DXVECTOR4(min, min, min, 1.0f);

	minimum = new D3DXVECTOR4(max, max, max, 1.0f);

	for (int i = 0; i < frustrumVertexes.size(); i++)
	{
		D3DXVec3Transform(frustrumVertexes[i], (D3DXVECTOR3*)frustrumVertexes[i], Game::Instance->WorldMM->GetCurrentMatrix());

		if (maximum->x < frustrumVertexes[i]->x)
			maximum->x = frustrumVertexes[i]->x;

		if (maximum->y < frustrumVertexes[i]->y)
			maximum->y = frustrumVertexes[i]->y;

		if (maximum->z < frustrumVertexes[i]->z)
			maximum->z = frustrumVertexes[i]->z;


		if (minimum->x > frustrumVertexes[i]->x)
			minimum->x = frustrumVertexes[i]->x;

		if (minimum->y > frustrumVertexes[i]->y)
			minimum->y = frustrumVertexes[i]->y;

		if (minimum->z > frustrumVertexes[i]->z)
			minimum->z = frustrumVertexes[i]->z;

		/*if (minimum->x > frustrumVertexes[i]->x && minimum->y > frustrumVertexes[i]->y && minimum->z > frustrumVertexes[i]->z)
			minimum = frustrumVertexes[i];*/
	}

	BoxfrustrumVertexes[0] = minimum;	//  <	<	<

	BoxfrustrumVertexes[1]->x = minimum->x;
	BoxfrustrumVertexes[1]->y = minimum->y;
	BoxfrustrumVertexes[1]->z = maximum->z;		//	<	<	>

	BoxfrustrumVertexes[2]->x = minimum->x;
	BoxfrustrumVertexes[2]->y = maximum->y;
	BoxfrustrumVertexes[2]->z = maximum->z;		//	<	>	>

	BoxfrustrumVertexes[3]->x = minimum->x;
	BoxfrustrumVertexes[3]->y = maximum->y;
	BoxfrustrumVertexes[3]->z = minimum->z;		//	<	>	<

	BoxfrustrumVertexes[4]->x = maximum->x;
	BoxfrustrumVertexes[4]->y = minimum->y;
	BoxfrustrumVertexes[4]->z = minimum->z;		//	>	<	<	

	BoxfrustrumVertexes[5]->x = maximum->x;
	BoxfrustrumVertexes[5]->y = minimum->y;
	BoxfrustrumVertexes[5]->z = maximum->z;		//	>	<	>

	BoxfrustrumVertexes[6]->x = maximum->x;
	BoxfrustrumVertexes[6]->y = maximum->y;
	BoxfrustrumVertexes[6]->z = minimum->z;		//	>	>	<

	BoxfrustrumVertexes[7] = maximum;	//	>	>	>

	c_height = abs(maximum->y - minimum->y);
	c_width = abs(maximum->x - minimum->x);
	c_lenght = abs(maximum->z - minimum->z);

	frustrumCenter = new D3DXVECTOR4(0, 0, 0, 0);

	for (int i = 0; i < BoxfrustrumVertexes.size(); i++)
	{
		frustrumCenter->x += BoxfrustrumVertexes[i]->x;
		frustrumCenter->y += BoxfrustrumVertexes[i]->y;
		frustrumCenter->z += BoxfrustrumVertexes[i]->z;
	}

	frustrumCenter->x /= BoxfrustrumVertexes.size();
	frustrumCenter->y /= BoxfrustrumVertexes.size();
	frustrumCenter->z /= BoxfrustrumVertexes.size();

}

void Camera::Update()
{

}

void Camera::Sleep()
{

}

void Camera::SetFOV(float FOV){
	_FOV = FOV;
}

void Camera::SetStartEnd(float start, float end){
	_start = start;
	_end = end;
}

