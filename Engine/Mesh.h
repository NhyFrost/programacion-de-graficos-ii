#pragma once

#include "Vertex.h"
#include "EngineApi.h"
#include <vector>

using namespace std;

#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_TEX1)

class ENGINE_API Mesh
{
public:
	LPDIRECT3DVERTEXBUFFER9 _vb;
	LPDIRECT3DINDEXBUFFER9 _ib;
	int _sizeV;
	int _sizeI;
	vector<Vertex> vertexes;

	Mesh(Vertex v[], int sizeV, WORD i[], int sizeI);
	~Mesh();
};