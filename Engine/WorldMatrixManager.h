#ifndef WORLDMATRIXMANAGER_H
#define WORLDMATRIXMANAGER_H

#include "EngineApi.h"
#include "MatrixManager.h"

class ENGINE_API WorldMatrixManager : public MatrixManager
{
public:
	void SetTransform(D3DXMATRIX* m) override;
	void MultiplyTransform(D3DXMATRIX* m) override;
	void Reset() override;
};

#endif // WORLDMATRIXMANAGER_H
