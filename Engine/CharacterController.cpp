#include "stdafx.h"
#include "CharacterController.h"
#include "GameObject.h"
#include "Input.h"

CharacterController::CharacterController()
{
	movementSpeed = 0.01f;
	rotationSpeed = 0.0001f;
}

void CharacterController::Update()
{
	if (Input::GetKey("arriba"))
		((GameObject*)parent)->transform->Translate(0, 0, movementSpeed);

	if (Input::GetKey("abajo"))
		((GameObject*)parent)->transform->Translate(0, 0, -movementSpeed);

	if (Input::GetKey("izquierda"))
		((GameObject*)parent)->transform->Translate(-movementSpeed, 0, 0);

	if (Input::GetKey("derecha"))
		((GameObject*)parent)->transform->Translate(movementSpeed, 0, 0);

	if (Input::GetKey("yaw+"))
		((GameObject*)parent)->transform->Rotate(0, rotationSpeed, 0);

	if (Input::GetKey("yaw-"))
		((GameObject*)parent)->transform->Rotate(0, -rotationSpeed, 0);

	if (Input::GetKey("roll+"))
		((GameObject*)parent)->transform->Rotate(0, 0, rotationSpeed);

	if (Input::GetKey("roll-"))
		((GameObject*)parent)->transform->Rotate(0, 0, -rotationSpeed);

	if (Input::GetKey("pitch+"))
		((GameObject*)parent)->transform->Rotate(rotationSpeed, 0, 0);

	if (Input::GetKey("pitch-"))
		((GameObject*)parent)->transform->Rotate(-rotationSpeed, 0, 0);
}