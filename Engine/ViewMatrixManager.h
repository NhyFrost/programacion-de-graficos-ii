#ifndef VIEWMATRIXMANAGER_H
#define VIEWMATRIXMANAGER_H

#include "EngineApi.h"
#include "MatrixManager.h"

class ENGINE_API ViewMatrixManager : public MatrixManager
{
public:
	void SetTransform(D3DXMATRIX* m) override;
	void MultiplyTransform(D3DXMATRIX* m) override;
	void Reset() override;
};

#endif // VIEWMATRIXMANAGER_H