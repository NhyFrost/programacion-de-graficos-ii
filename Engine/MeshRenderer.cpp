#include "stdafx.h"
#include "MeshRenderer.h"
#include "Game.h"


MeshRenderer::MeshRenderer(Mesh* mesh, Material* material)
{
	_mesh = mesh;
	_material = material;

	for (int i = 0; i < _mesh->vertexes.size(); i++)
	{
		D3DXVECTOR4* aux = new D3DXVECTOR4();
		transformedVertexes.push_back(aux);
	}

	for (int i = 0; i < 8; i++)
	{
		D3DXVECTOR4* aux = new D3DXVECTOR4(0, 0, 0, 0);
		boundingBox.push_back(aux);
	}

	maximum = new D3DXVECTOR4(0, 0, 0, 0);
	minimum = new D3DXVECTOR4(0, 0, 0, 0);
	center = new D3DXVECTOR4(0, 0, 0, 0);
	camCenter = new D3DXVECTOR4(0, 0, 0, 0);
}

void MeshRenderer::Draw()
{
	Camera* cam = ((Scene*)(parent->rootParent))->mainCamera;

	for (int i = 0; i < _mesh->vertexes.size(); i++)
	{
		D3DXVec3Transform(transformedVertexes[i], &(_mesh->vertexes[i].coord3D), Game::Instance->WorldMM->GetCurrentMatrix());

		if (maximum->x < transformedVertexes[i]->x)
			maximum->x = transformedVertexes[i]->x;

		if (maximum->y < transformedVertexes[i]->y)
			maximum->y = transformedVertexes[i]->y;

		if (maximum->z < transformedVertexes[i]->z)
			maximum->z = transformedVertexes[i]->z;


		if (minimum->x > transformedVertexes[i]->x)
			minimum->x = transformedVertexes[i]->x;

		if (minimum->y > transformedVertexes[i]->y)
			minimum->y = transformedVertexes[i]->y;

		if (minimum->z > transformedVertexes[i]->z)
			minimum->z = transformedVertexes[i]->z;
	}

	boundingBox[0] = minimum;	//  <	<	<

	boundingBox[1]->x = minimum->x;
	boundingBox[1]->y = minimum->y;
	boundingBox[1]->z = maximum->z;		//	<	<	>

	boundingBox[2]->x = minimum->x;
	boundingBox[2]->y = maximum->y;
	boundingBox[2]->z = maximum->z;		//	<	>	>

	boundingBox[3]->x = minimum->x;
	boundingBox[3]->y = maximum->y;
	boundingBox[3]->z = minimum->z;		//	<	>	<

	boundingBox[4]->x = maximum->x;
	boundingBox[4]->y = minimum->y;
	boundingBox[4]->z = minimum->z;		//	>	<	<	

	boundingBox[5]->x = maximum->x;
	boundingBox[5]->y = minimum->y;
	boundingBox[5]->z = maximum->z;		//	>	<	>

	boundingBox[6]->x = maximum->x;
	boundingBox[6]->y = maximum->y;
	boundingBox[6]->z = minimum->z;		//	>	>	<

	boundingBox[7] = maximum;	//	>	>	>

	camCenter = cam->frustrumCenter;

	center->x = 0;
	center->y = 0;
	center->z = 0;

	for (int i = 0; i < boundingBox.size(); i++)
	{
		center->x += boundingBox[i]->x;
		center->y += boundingBox[i]->y;
		center->z += boundingBox[i]->z;
	}

	center->x /= boundingBox.size();
	center->y /= boundingBox.size();
	center->z /= boundingBox.size();


	m_height = abs(maximum->y - minimum->y);
	m_width = abs(maximum->x - minimum->x);
	m_lenght = abs(maximum->z - minimum->z);

	float dx = abs(center->x - camCenter->x);
	float dy = abs(center->y - camCenter->y);
	float dz = abs(center->z - camCenter->z);

	float w = (cam->c_width + m_width) / 2;
	float h = (cam->c_height + m_height) / 2;
	float l = (cam->c_lenght + m_lenght) / 2;

	if (dx < w && dy < h && dz < l)
	{
		int outOfFrustrumCounter;
		for (int i = 0; i < cam->frustrum.size(); i++)
		{
			outOfFrustrumCounter = 0;
			for (int j = 0; j < boundingBox.size(); j++) //los vertices del bounding box
			{
				float value = D3DXPlaneDotCoord(cam->frustrum[i], (D3DXVECTOR3*)boundingBox[j]);

				if (value < 0)
					outOfFrustrumCounter++;
			}

			if (outOfFrustrumCounter == boundingBox.size())
			{
				ShouldDraw = false;
				break;
			}
		}

		_material->SetMaterial();

		Game::Instance->_dev->SetFVF(CUSTOMFVF);
		Game::Instance->_dev->SetIndices(_mesh->_ib);
		Game::Instance->_dev->SetStreamSource(0, _mesh->_vb, 0, sizeof(Vertex));
		Game::Instance->_dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, _mesh->_sizeV, 0, _mesh->_sizeI / 3);
	}
	else
	{
		int x;
		x = 10;
	}


	//Multiplicar los vertices del mesh por la matriz de mundo para tener la caja de colision
	//la camara tiene los 6 planos del frustrum
	//comparo contra esos planos, asi todos los vertices estan en negativo = fuera del frustrum
	//caso contrario, si por lo menos un vertice esta dentro de los 8 planos(es positivo) se dibuja el objeto y re pite lo mismo con los hijos
	// ---DONE---


	//sacar el vector maximo y minimo
	//crear el resto de vertices(8)
	//chequear lo mismo que con los planos, pero con los vertices de la caja

	//TODO:
	//Conseguir maximo y minimo del frustrum(5 vertices)


}
