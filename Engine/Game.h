#pragma once

#include "Camera.h"
#include "MeshRenderer.h"
#include "EngineApi.h"
#include "Input.h"
#include "WorldMatrixManager.h"
#include "ViewMatrixManager.h"
#include "Scene.h"


#include <chrono>

using namespace std;

class ENGINE_API Game
{
public:
	static Game* Instance;

	Input input;

	HINSTANCE _hInstance;
	HWND _hWnd;
	int _nCmdShow;
	float num;

	Scene* currentScene;

	WorldMatrixManager* WorldMM;
	ViewMatrixManager* ViewMM;

	float deltaTime;

	UINT height;
	UINT width;

	LPDIRECT3D9 _d3d;

	LPDIRECT3DDEVICE9 _dev;

	Game(HINSTANCE hInstance);
	~Game();

	void Start(int nCmdShow);
	void Draw();
	virtual void Update();
};