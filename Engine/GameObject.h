#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "NodeWithChildren.h"
#include "EngineApi.h"
#include "Transform.h"

class ENGINE_API GameObject : public NodeWithChildren
{
public:
	Transform* transform = new Transform();
	Node* GetChildByName(string name);

	void Awake() override;
	void Update() override;
	void Draw() override;
	void Sleep() override;
};


#endif //GAMEOBJECT_H