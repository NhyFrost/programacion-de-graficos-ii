#include "stdafx.h"
#include "MatrixManager.h"
#include "Game.h"


void MatrixManager::Push(D3DXMATRIX* m)
{
	Matrices.push_back(*m);
}

void MatrixManager::Pop()
{
	if (Matrices.size() > 0)
	{
		*aux = Matrices[Matrices.size() - 1];
		Matrices.pop_back(); 
		Game::Instance->WorldMM->SetTransform(aux);
	}
}

D3DXMATRIX* MatrixManager::GetCurrentMatrix()
{
	if (Matrices.size() > 0)
	{
		*aux = Matrices[Matrices.size() - 1];
		return aux;
	}
	D3DXMatrixIdentity(aux);
	return aux;
}