#ifndef MATRIXMANAGER_H
#define MATRIXMANAGER_H

#include "EngineApi.h"

using namespace std;

class ENGINE_API MatrixManager
{
protected:
	vector<D3DXMATRIX> Matrices;
public:
	D3DXMATRIX* aux = new D3DXMATRIX();
	virtual void SetTransform(D3DXMATRIX* m) = 0;
	virtual void MultiplyTransform(D3DXMATRIX* m) = 0;
	void Push(D3DXMATRIX* m);
	void Pop();
	D3DXMATRIX* GetCurrentMatrix();
	virtual void Reset() = 0;
};


#endif //MATRIXMANAGER_H