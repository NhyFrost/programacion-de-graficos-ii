#include "stdafx.h"
#include "ViewMatrixManager.h"
#include "Game.h"

void ViewMatrixManager::SetTransform(D3DXMATRIX* m)
{
	Game::Instance->_dev->SetTransform(D3DTS_VIEW, m);
}

void ViewMatrixManager::MultiplyTransform(D3DXMATRIX* m)
{
	Game::Instance->_dev->MultiplyTransform(D3DTS_VIEW, m);
}

void ViewMatrixManager::Reset()
{
	//D3DXMATRIX* aux = new D3DXMATRIX();

	Matrices.clear();
	D3DXMatrixIdentity(aux);

	Game::Instance->_dev->SetTransform(D3DTS_VIEW, aux);
}