#pragma once

#include "EngineApi.h"

class ENGINE_API Transform
{
public:

	D3DXVECTOR3 pos;
	D3DXVECTOR3 rot;
	D3DXVECTOR3 sca;

	D3DXVECTOR3 forward;
	D3DXVECTOR3 up;
	D3DXVECTOR3 right;

	Transform();
	~Transform();

	void Translate(float x, float y, float z);
	void Scale(float x, float y, float z);
	void Rotate(float x, float y, float z);

	D3DXMATRIX GetModel();
};