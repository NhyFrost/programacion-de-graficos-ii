#ifndef CAMERA_H
#define CAMERA_H

#include "Transform.h"
#include "EngineApi.h"
#include "Component.h"

class ENGINE_API Camera : public Component
{
public:
	float _FOV;
	float _start;
	float _end;

	//array de planos
	vector<D3DXPLANE*> frustrum;

	vector<D3DXVECTOR4*> frustrumVertexes;
	vector<D3DXVECTOR4*> BoxfrustrumVertexes;


	float c_height, c_width, c_lenght;

	D3DXVECTOR3 middleRightPoint;

	D3DXVECTOR4* frustrumCenter;
	D3DXVECTOR4* frustrumMaximum;
	D3DXVECTOR4* frustrumMinimum;

	D3DXVECTOR4	*maximum, *minimum;

	Camera();
	~Camera();

	void SetFOV(float _FOV);
	void SetStartEnd(float start, float end);

	void SetCamLookPos(float x, float y, float z);
	void SetCamUp(float x, float y, float z);

	void Update() override;
	void Draw() override;
	void Sleep() override;
};

#endif //CAMERA_H