#include "stdafx.h"
#include "Input.h"
#include "Game.h"
#include <algorithm>

LPDIRECTINPUT8 Input::dinp = nullptr;
LPDIRECTINPUTDEVICE8 Input::keyDev = nullptr;
std::map<std::string, std::vector<int>> Input::_keys;
byte Input::prevKeys[256];
byte Input::currentKeys[256];

void Input::InitInputKeyboard() {
	//Inicializamos DirectInput
	
	DirectInput8Create(Game::Instance->_hInstance,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&dinp,
		NULL);

	
	dinp->CreateDevice(GUID_SysKeyboard, &keyDev, NULL);
	keyDev->SetDataFormat(&c_dfDIKeyboard);
	keyDev->SetCooperativeLevel(Game::Instance->_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	keyDev->Acquire();
}

void Input::SetKey(std::string actionName, int key) {
	_keys[actionName].push_back(key);
}

bool Input::GetKey(std::string actionName) {
	//if (_keys)
	//{
		std::vector<int> keys = _keys.find(actionName)->second;
		for (size_t i = 0; i < keys.size(); i++)
			if (currentKeys[keys[i]])
				return true;
		return false;
	//}
	//return false;
}

bool Input::GetKeyDown(std::string actionName) {
	std::vector<int> keys = _keys.find(actionName)->second;
	for (size_t i = 0; i < keys.size(); i++)
		if (currentKeys[keys[i]])
			return true;
	return false;
}

bool Input::GetKeyUp(std::string actionName) {
	std::vector<int> keys = _keys.find(actionName)->second;
	for (size_t i = 0; i < keys.size(); i++)
		if (currentKeys[keys[i]])
			return true;
	return false;
}

void Input::UpdateKeyboard() {
	for (size_t i = 0; i < 256; i++)
	{
		prevKeys[i] = currentKeys[i];
	}
	keyDev->GetDeviceState(sizeof(currentKeys), &currentKeys);
}

void Input::FreeResources() {
	keyDev->Unacquire();
	keyDev->Release();
	dinp->Release();
}