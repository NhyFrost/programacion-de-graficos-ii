#ifndef NODEWITHCHILDREN_H
#define NODEWITHCHILDREN_H

#include "Node.h"
#include "EngineApi.h"

using namespace std;

class ENGINE_API NodeWithChildren : public Node
{
public:
	vector<Node*> children;
};

#endif //NODEWITHCHILDREN_H