#include "stdafx.h"
#include "Texture.h"
#include "Game.h"


Texture::Texture(LPCWSTR dirTextura, D3DTEXTUREADDRESS adress, D3DTEXTUREFILTERTYPE filter)
{
	D3DXCreateTextureFromFile(Game::Instance->_dev, dirTextura, &texture);
	_adress = adress;
	_filter = filter;
}


Texture::~Texture()
{
}

void Texture::SetAdress(D3DTEXTUREADDRESS adress){
	_adress = adress;
}
void Texture::SetFilter(D3DTEXTUREFILTERTYPE filter){
	_filter = filter;
}

void Texture::SetAdressFilter(int stage){
	Game::Instance->_dev->SetTexture(stage, texture);
	Game::Instance->_dev->SetSamplerState(stage, D3DSAMP_ADDRESSU, _adress);
	Game::Instance->_dev->SetSamplerState(stage, D3DSAMP_ADDRESSV, _adress);
	Game::Instance->_dev->SetSamplerState(stage, D3DSAMP_MAGFILTER, _filter);
	Game::Instance->_dev->SetSamplerState(stage, D3DSAMP_MINFILTER, _filter);
	Game::Instance->_dev->SetSamplerState(stage, D3DSAMP_MIPFILTER, _filter);
}
