#pragma once

#include "EngineApi.h"

class ENGINE_API Vertex{
public:
	FLOAT _x, _y, _z;
	FLOAT _tu, _tv;
	D3DXVECTOR3 coord3D;
	D3DXVECTOR2 coord2D;
	Vertex(){};
	Vertex(FLOAT x, FLOAT y, FLOAT z, FLOAT tu, FLOAT tv);
	~Vertex();
};

