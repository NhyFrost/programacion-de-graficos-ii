#ifndef MESHRENDERER_H
#define MESHRENDERER_H

#include "Component.h"
#include "Mesh.h"
#include "Transform.h"
#include "Material.h"
#include "EngineApi.h"

class ENGINE_API MeshRenderer : public Component
{
public:
	Mesh* _mesh;
	Material* _material;
	vector<D3DXVECTOR4*> transformedVertexes, boundingBox;
	D3DXVECTOR4	*maximum, *minimum, *camCenter, *center;
	bool ShouldDraw = false;

	float m_height, m_width, m_lenght;

	MeshRenderer(Mesh* mesh, Material* material);

	void Draw() override;
};

#endif //MESHRENDERER_H
