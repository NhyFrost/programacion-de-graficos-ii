#include "stdafx.h"
#include "Mesh.h"
#include "Game.h"

Mesh::Mesh(Vertex v[], int sizeV, WORD i[], int sizeI)
{
	for (int i = 0; i < sizeV; i++)
	{
		vertexes.push_back(v[i]);
	}

	_sizeV = sizeV;
	_sizeI = sizeI;

	HRESULT hr = Game::Instance->_dev->CreateVertexBuffer(sizeof(Vertex) * sizeV, 0, CUSTOMFVF, D3DPOOL_MANAGED, &_vb, NULL);
	hr = Game::Instance->_dev->CreateIndexBuffer(sizeI * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &_ib, NULL);

	VOID *lockedData = NULL;
	_vb->Lock(0, sizeof(Vertex)* sizeV, (void**)&lockedData, 0);
	memcpy(lockedData, v, sizeof(Vertex) * sizeV);
	_vb->Unlock();

	_ib->Lock(0, sizeI * sizeof(WORD), (void**)&lockedData, 0);
	memcpy(lockedData, i, sizeof(WORD) * sizeI);
	_ib->Unlock();
}

Mesh::~Mesh(){

}