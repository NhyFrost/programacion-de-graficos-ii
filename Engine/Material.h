#pragma once

#include "EngineApi.h"

class ENGINE_API Material
{
public:

	Material();
	~Material();

	virtual void SetMaterial();
};

