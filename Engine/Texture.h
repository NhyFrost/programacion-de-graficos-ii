#pragma once

#include "EngineApi.h"

class ENGINE_API Texture
{
public:
	LPDIRECT3DTEXTURE9 texture;
	D3DTEXTUREADDRESS _adress;
	D3DTEXTUREFILTERTYPE _filter;

	Texture(LPCWSTR direccion, D3DTEXTUREADDRESS adress, D3DTEXTUREFILTERTYPE filter);
	~Texture();

	void SetAdressFilter(int stage);

	void SetAdress(D3DTEXTUREADDRESS adress);
	void SetFilter(D3DTEXTUREFILTERTYPE filter);
};
