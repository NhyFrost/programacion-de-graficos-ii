#ifndef NODE_H
#define NODE_H

#include "EngineApi.h"
#include <string>

using namespace std;

class ENGINE_API Node
{
public:
	string name;

	bool isUpdating = false;
	Node* parent;
	Node* rootParent;

	virtual void Awake() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Sleep() = 0;
};

#endif //NODE_H