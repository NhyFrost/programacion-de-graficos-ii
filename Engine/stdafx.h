// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <d3d9.h> //Busca el header de directx en los path
#include <d3dx9.h> // Busca el header de directx en los path
#include <dinput.h>
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#pragma comment (lib, "d3dx9.lib") //Incluyo la lib a mi proyecto
#pragma comment (lib, "dinput8.lib") //Incluyo la lib a mi proyecto
#pragma comment (lib, "dxguid.lib") //Incluyo la lib a mi proyecto
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <stdlib.h>
#include <vector>
#include <map>


