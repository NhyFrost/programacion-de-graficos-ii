#ifndef COMPONENT_H
#define COMPONENT_H

#include "Node.h"
#include "EngineApi.h"

class ENGINE_API Component : public Node
{
	virtual void Awake() override;
	virtual void Update() override;
	void Draw() override;
	virtual void Sleep() override;
};


#endif // COMPONENT_H