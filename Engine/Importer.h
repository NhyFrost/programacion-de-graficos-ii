#ifndef IMPORTER_H
#define IMPORTER_H

#include "EngineApi.h"
#include "Vertex.h"
#include "Mesh.h"

using namespace std;

class ENGINE_API Importer
{
	vector<Vertex> finalVertexes;

	vector<WORD> indexes;

public:
	bool LoadOBJ(const char* path, Mesh** m); //puede pasar el mesh como referencia o un puntero a puntero null
};


#endif//IMPORTER_H