#include "stdafx.h"
#include "Component.h"
#include "Game.h"
#include "GameObject.h"

void Component::Awake()
{
	isUpdating = true;
}

void Component::Draw()
{

}

void Component::Update()
{

}

void Component::Sleep()
{

}