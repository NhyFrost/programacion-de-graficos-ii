#ifndef SCENE_H
#define SCENE_H

#include "GameObject.h"
#include "EngineApi.h"
#include "Transform.h"
#include "Camera.h"

class ENGINE_API Scene : public GameObject
{
public:
	Camera* mainCamera;
	void Sleep() override;

	void LoadIdentity();

};

#endif // SCENE_H