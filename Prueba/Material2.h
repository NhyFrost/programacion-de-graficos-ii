#pragma once
#include "Texture.h"
#include "Material.h"

class Material2 :
	public Material
{
private:
	Texture* _tex1;
	Texture* _tex2;
public:
	Material2(Texture* tex1, Texture* tex2);
	~Material2();

	void SetTexture1(Texture* tex);
	void SetTexture2(Texture* tex);

	void SetMaterial() override;
};