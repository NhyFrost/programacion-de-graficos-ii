#include "stdafx.h"
#include "MiJuego.h"
#include "Material1.h"
#include "Material2.h"
#include "MaterialTransparencia.h"
#include "Importer.h"
#include "ViewMatrixManager.h"
#include "WorldMatrixManager.h"
#include "Scene.h"
#include "GameObject.h"
#include "CharacterController.h"

#include <iostream>

MiJuego::MiJuego(HINSTANCE hInstance) : Game(hInstance)
{
	
#pragma region normalMesh
	Vertex vertexes[] =
	{
		{ -0.5f, -0.5f, 0.0f, 0.0f, 1.0f },
		{ 0.5f, -0.5f, 0.0f, 1.0f, 1.0f },
		{ 0.5f, 0.5f, 0.0f, 1.0f, 0.0f },
		{ -0.5f, 0.5f, 0.0f, 0.0f, 0.0f }
	};

	WORD indexes[] = { 3, 0, 1, 3, 1, 2 };

	Mesh* mesh = new Mesh(vertexes, 4, indexes, 6);

	Vertex vertexes1[] =
	{
		{ 0.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ 0.5f, -0.5f, 0.0f, 1.0f, 0.0f },
		{ -0.5f, -0.5f, 0.0f, 0.0f, 0.0f }
	};

	WORD indexes1[] = { 2, 0, 1 };

	Mesh* mesh1 = new Mesh(vertexes1, 3, indexes1, 3);

	Vertex vertexes2[] =
	{
		{ 0.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ -0.5f, 0.5f, 0.0f, 1.0f, 0.0f },
		{ 0.5f, 0.5f, 0.0f, 0.0f, 0.0f }
	};

	WORD indexes2[] = { 0, 1, 2 };
	Mesh* mesh2 = new Mesh(vertexes2, 3, indexes2, 3);
#pragma endregion
	
#pragma region Textures
	Texture* texture = new Texture(L"metal.jpg", D3DTADDRESS_MIRROR, D3DTEXF_ANISOTROPIC);
	Texture* textura2 = new Texture(L"madera.jpg", D3DTADDRESS_MIRROR, D3DTEXF_ANISOTROPIC);
	Texture* textura3 = new Texture(L"tierra.jpg", D3DTADDRESS_MIRROR, D3DTEXF_ANISOTROPIC);
	Texture* sprite = new Texture(L"sprites.png", D3DTADDRESS_MIRROR, D3DTEXF_ANISOTROPIC);
#pragma endregion
	
#pragma region Materials
	Material* mat1 = new Material1(texture, textura2);
	Material* mat2 = new Material2(texture, textura2);
	Material* matT = new MaterialTransparencia(textura3);
#pragma endregion

#pragma region OBJMesh

	Mesh* objMesh;

	Importer* imp = new Importer();

	imp->LoadOBJ("cube2.obj", &objMesh);

#pragma endregion

#pragma region SceneGraph

	Scene* scene = new Scene();
	scene->LoadIdentity();

	GameObject* cameraObj = new GameObject();
	cameraObj->name = "cameraObj";

	Camera* camera = new Camera();

	CharacterController* CC = new CharacterController();

	GameObject* obj = new GameObject();
	obj->name = "obj";

	MeshRenderer* meshRenderer = new MeshRenderer(objMesh,mat2);

	//obj->transform->pos.x = -5;


	//add children
	cameraObj->children.push_back(camera);
	cameraObj->children.push_back(CC);
	scene->children.push_back(cameraObj);
	scene->children.push_back(obj);
	obj->children.push_back(meshRenderer);


	//add rootParents
	cameraObj->rootParent = scene;
	obj->rootParent = scene;

	//add parents
	camera->parent = cameraObj;
	CC->parent = cameraObj;
	meshRenderer->parent = obj;
	cameraObj->parent = scene;
	obj->parent = scene;

	//obj->transform->Scale(10,10,10);
	//obj->transform->Translate(0,0,0);

	cameraObj->transform->Translate(0, 0, 0);
	cameraObj->transform->pos.z = -4;
	cameraObj->transform->Rotate(0,D3DXToRadian(0),0.00f);

#pragma endregion

#pragma region Inputs
	Input::SetKey("arriba", DIK_UP);
	Input::SetKey("arriba", DIK_W);
	Input::SetKey("abajo", DIK_DOWN);
	Input::SetKey("abajo", DIK_S);
	Input::SetKey("izquierda", DIK_LEFT);
	Input::SetKey("izquierda", DIK_A);
	Input::SetKey("derecha", DIK_RIGHT);
	Input::SetKey("derecha", DIK_D);
	Input::SetKey("q", DIK_Q);
	Input::SetKey("e", DIK_E);

	Input::SetKey("yaw-", DIK_J);
	Input::SetKey("yaw+", DIK_L);
	Input::SetKey("pitch-", DIK_I);
	Input::SetKey("pitch+", DIK_K);
	Input::SetKey("roll-", DIK_U);
	Input::SetKey("roll+", DIK_O);
#pragma endregion
}

MiJuego::~MiJuego()
{
}
