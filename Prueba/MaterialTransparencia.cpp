#include "stdafx.h"
#include "MaterialTransparencia.h"
#include "Game.h"


MaterialTransparencia::MaterialTransparencia(Texture* tex1)
{
	_tex1 = tex1;
}


MaterialTransparencia::~MaterialTransparencia()
{
}

void MaterialTransparencia::SetMaterial(){

	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT1);

	Game::Instance->_dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	Game::Instance->_dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
	Game::Instance->_dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);

	_tex1->SetAdressFilter(0);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	
	for (size_t i = 2; i < 7; i++)
		Game::Instance->_dev->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);

}

void MaterialTransparencia::SetTexture1(Texture* tex){
	_tex1 = tex;
}

