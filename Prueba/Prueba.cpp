// Test.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Prueba.h"
#include "MiJuego.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	MiJuego* mg = new MiJuego(hInstance);
	mg->Start(nCmdShow);
}
