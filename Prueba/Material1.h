#pragma once
#include "Texture.h"
#include "Material.h"

class Material1 :
	public Material
{
private:
	Texture* _tex1;
	Texture* _tex2;
public:

	Material1(Texture* tex1, Texture* tex2);
	~Material1();

	void SetTexture1(Texture* tex);
	void SetTexture2(Texture* tex);

	void SetMaterial() override;
};

