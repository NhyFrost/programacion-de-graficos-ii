#pragma once
#include "Texture.h"
#include "Material.h"

class MaterialTransparencia :
	public Material
{
public:
	Texture* _tex1;

	MaterialTransparencia(Texture* tex1);
	~MaterialTransparencia();

	void SetTexture1(Texture* tex);

	void SetMaterial() override;
};

