#include "stdafx.h"
#include "Material1.h"
#include "Game.h"


Material1::Material1(Texture* tex1, Texture* tex2)
{
	_tex1 = tex1;
	_tex2 = tex2;
}


Material1::~Material1()
{
}

void Material1::SetMaterial(){

	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT1);
	Game::Instance->_dev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

	_tex1->SetAdressFilter(0);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	Game::Instance->_dev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);

	
	_tex2->SetAdressFilter(1);
	Game::Instance->_dev->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_ADD);
	Game::Instance->_dev->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	Game::Instance->_dev->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_CURRENT);
	Game::Instance->_dev->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 0);

	for (size_t i = 2; i < 7; i++)
		Game::Instance->_dev->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);

}

void Material1::SetTexture1(Texture* tex){
	_tex1 = tex;
}

void Material1::SetTexture2(Texture* tex){
	_tex2 = tex;
}
